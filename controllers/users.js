const path = require('path');
const User = require('../models/users');
const md5 = require('md5');

exports.create = async (req, res) => {
	req.body.contrasena = md5(req.body.contrasena); // Encrypt MD5 Password
	try {
		const newUser = await User.save();
		return res.status(201).send(res.json(newUser))
	} catch (error) {
		res.status(500).send(error);	
	}
};


exports.update = async (req, res) => {
    let userId = req.params.id || null;
	try {
		const updateUser = await User.findByIdAndUpdate(userId, req.body, { new: true });
		if(!updateUser){
			return res.status(404).send('User no existe');
		}
		res.json(updateUser);
	} catch (error) {
		res.status(500).send(error);
	}
};


exports.list = async (req, res) => {
	
	try{
		const users = await User.find();
		res.json(users);
	}catch(error){
		res.status(500).send(error);
	}
};


exports.byId = function (req, res) {
	let userId = req.params.id || null;
	if(userId == null) return res.status(404).send({message: 'User ID no Valid.'});
	User.findById(userId)
	.then(function (err, user) {
		if (err) {
			res.status(400).send({message: err});
		}
		return res.status(200).send({user});
	}).catch(function (err) {
		console.log(err)
	});
};


exports.softDelete = function (req, res) {
	let userId = req.params.id || null;
if(userId == null) return res.status(404).send({message: 'User ID no Valid.'});
console.log(req.body);
User.findByIdAndUpdate(userId, { isActive: false}, { new:true }, (err, user) => {
		if(err) {
			console.error('Error: ', err)
			//res.send({message: err});
			res.status(400).send({message: err});
		}
		console.log(user)
		//res.send(data)
		return res.status(201).send({user});
	});
};


