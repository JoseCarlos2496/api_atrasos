require('dotenv').config();

var createError = require('http-errors');
var express = require('express');
var jwt = require('jsonwebtoken');
var path = require('path');
const md5 = require('md5');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

// Models
var User = require('./models/users');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var atrasosRouter = require('./routes/atrasos');

var app = express();
require('./config/db');

const port = process.env.PORT || 8080;


// Middlewares
app.use(express.json());
app.use(express.urlencoded({ extended: true }));


// CORS
app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
  res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
  res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
  next();
});


// // Auth Process
// app.set('key', 'SM4RNkdmns844nddFQ');

// app.post('/auth', (req, res) => {

//   var email = req.body.email || '';
//   var password = md5(req.body.password || '');
//   var where = {email:email, password:password};

//   User.findOne(where, (err, user) => {
//     if(err) return res.status(500).send({message: 'Authenticate error.'});
//     if(!user) return res.status(403).send({message: 'Authenticate error.'});

//     const token = jwt.sign({check: true}, app.get('key'), {expiresIn: "2h"});

//     res.status(200).send({
//         message: 'Auth succesfully',
//         token: token,
//         user: user,
//     });
//   });
// });


// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');


app.listen(port, function () {
  console.log(`Example app listening on ${port}!`)
})


app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

//app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/atrasos', atrasosRouter);
app.get('/ruta', (req, res) => {
  return res.send('Respuesta enviada');
});

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// // error handler
// app.use(function(err, req, res, next) {
//   // set locals, only providing error in development
//   res.locals.message = err.message;
//   res.locals.error = req.app.get('env') === 'development' ? err : {};

//   // render the error page
//   res.status(err.status || 500);
//   res.render('error');
// });

module.exports = app;
