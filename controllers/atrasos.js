let Atraso = require('../models/atrasos');

exports.create = function (req, res) {
    let newAtraso = new Atraso(req.body);
    newAtraso.save()
        .then(data => {
            res.status(201).send(data)
        }).catch(err => {
            res.status(400).send({message: err});
        });
};

exports.update = function (req, res) {
    let atrasoId = req.params.id || null;
    if(atrasoId == null) return res.status(404).send({message: 'Atraso ID no Valid.'});
    
    Atraso.findByIdAndUpdate(atrasoId, req.body, { new:true }, (err, data) => {
        if(err) {
            console.error('Error: ', err)
            //res.send({message: err});
            res.status(400).send({message: err});
        }
        console.log(data)
        //res.send(data)
        return res.status(201).send({data}); 
    });
};

exports.list = function (req, res) {
    Atraso.find()
        .then(function (err, Atraso) {
            if (err) {
                    res.status(400).send({message: err});
            }
            return res.status(200).send({
                    Atraso
            });
        }).catch(function (err) {
            console.log(err)
        });
};

exports.byId = function (req, res) {
    let atrasoId = req.params.id || null;
    Atraso.findById(atrasoId)
        .then(function (err, Atraso) {
            if (err) {
                res.status(400).send({message: err});
            }
            return res.status(200).send({Atraso});
        }).catch(function (err) {
            console.log(err)
        });
};

exports.delete = function (req, res) {
        let atrasoId = req.params.id || null;
        if (atrasoId == null) return res.status(404).send({message: 'atrasoId No valid'}); 
        Atraso.findByIdAndDelete(atrasoId, function (err, Atraso) {
                if (err) {
                        res.status(400).send({message: err});
                }
                return res.status(200).send({message: 'Atraso deleted successfully'});
        });
};

exports.softDelete = function (req, res) {
    let atrasoId = req.params.id || null;
    if(atrasoId == null) 
        return res.status(404).send({message: 'Atraso ID no Valid.'});

    Atraso.findByIdAndUpdate(atrasoId, { isActive:false }, { new:true }, function (err, Atraso) {
            if(err) {
                res.status(400).send({message: err});
            }
            return res.status(201).send({Atraso}); 
    });
};
