var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Users = Schema({
    nombre: {type: String, require: true},
    correo: {type: String, require: true},
    contrasena: { type: String, required: true},
    nombreCompleto: {type:String, required: true},
    isActive: { type: Boolean, default: true },
    createdAt: {type: Date, default: Date.now},
    updatedAt: {type: Date, default: Date.now}
});

module.exports = mongoose.model('Users', Users);
