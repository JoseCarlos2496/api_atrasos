const express = require('express');
const router = express.Router();
const atraso = require('../controllers/atrasos');

/* GET home page. */
// router.get('/', function(req, res, next) {
//     //res.render('index', { title: 'Express' });
//   });

router.get('/', function(req, res){
  atraso.list(req,res);
});

router.get('/:id', function(req, res) {
  atraso.byId(req,res);
});

router.post('/', function(req, res) {
  atraso.create(req,res);
});

router.put('/:id', function(req, res) {
  atraso.update(req,res);
});

router.delete('/:id', function(req, res) {
  atraso.delete(req,res);
});

router.delete('/Soft/:id', function(req, res) {
  atraso.delete(req,res);
});

module.exports = router;