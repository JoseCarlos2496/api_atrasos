const express = require('express');
const router = express.Router();
const user = require('../controllers/users');

/* GETAll users listing. */
router.get('/', function(req, res){
  user.list(req,res);
});

/* GETById users listing. */
router.get('/:id', function(req, res) {
  user.byId(req,res);
});

/* POST users listing. */
router.post('/', function(req, res) {
  user.create(req,res);
});

/* PUT users listing. */
router.put('/:id', function(req, res) {
  user.update(req,res);
});

/* DELETE users listing. */
router.delete('/Soft/:id', function(req, res) {
  user.softDelete(req,res);
});


// /* GETById users listing. */
// router.get('/', function(req, res) {
//   res.send('Hola mundo');
// });

module.exports = router;