//var EstadoAtraso = require('estadoAtraso');
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Atrasos = Schema({
    nombre: {type: String, require: true},
    permiso: {type: Boolean, require: true, default: false},
    fechaHora: {type:Date, default: Date.now()},
    valor: {type: mongoose.Schema.Types.Decimal128, require: true, default:0.5},
    estadoDeuda: {type: String, require: true, default:'Debe'},
    isActive: { type: Boolean, default: true },
    createdAt: {type: Date, default: Date.now},
    updatedAt: {type: Date, default: Date.now}
});

module.exports = mongoose.model('Atrasos', Atrasos);
